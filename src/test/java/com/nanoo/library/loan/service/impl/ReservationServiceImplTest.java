package com.nanoo.library.loan.service.impl;

import com.nanoo.library.commonpackage.exception.FunctionalException;
import com.nanoo.library.commonpackage.model.Status;
import com.nanoo.library.loan.database.BookRepository;
import com.nanoo.library.loan.database.ClientRepository;
import com.nanoo.library.loan.database.LoanRepository;
import com.nanoo.library.loan.database.ReservationRepository;
import com.nanoo.library.loan.model.dto.BookDto;
import com.nanoo.library.loan.model.dto.ClientDto;
import com.nanoo.library.loan.model.dto.ReservationDto;
import com.nanoo.library.loan.model.entities.Book;
import com.nanoo.library.loan.model.entities.Client;
import com.nanoo.library.loan.model.entities.CopyBook;
import com.nanoo.library.loan.model.entities.Loan;
import com.nanoo.library.loan.model.mapper.BookMapper;
import com.nanoo.library.loan.model.mapper.ClientMapper;
import com.nanoo.library.loan.model.mapper.ReservationMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class ReservationServiceImplTest {

    @Mock ReservationRepository reservationRepository;
    @Mock ClientRepository clientRepository;
    @Mock ReservationMapper reservationMapper;
    @Mock ClientMapper clientMapper;
    @Mock LoanRepository loanRepository;
    @Mock BookMapper bookMapper;
    @Mock BookRepository bookRepository;

    private ReservationServiceImpl reservationServiceUnderTest;

    @BeforeEach
    public void initMock() {
        reservationServiceUnderTest = new ReservationServiceImpl(reservationRepository, clientRepository, reservationMapper, clientMapper, loanRepository, bookMapper, bookRepository);
    }

    public void initSecurityMock() {
        Authentication authentication = mock(Authentication.class, Mockito.RETURNS_DEEP_STUBS);
        SecurityContext securityContext = mock(SecurityContext.class, Mockito.RETURNS_DEEP_STUBS);
        given(securityContext.getAuthentication()).willReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        given(SecurityContextHolder.getContext().getAuthentication().getName()).willReturn("user@mail.fr");
    }

    @DisplayName("Test good validation of reservation - Max number")
    @Test
    void checkReservationValidity_NumberMaxOfReservation() {
        // GIVEN
        given(reservationRepository.getNumberOfReservation(1)).willReturn(6);
        BookDto bookDto = new BookDto();
        ReservationDto reservationDto = new ReservationDto();
        bookDto.setId(1);
        bookDto.setCopies(3);
        reservationDto.setBook(bookDto);

        // WHEN
        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> reservationServiceUnderTest.checkReservationValidity(reservationDto));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("Le nombre maximal de réservations pour ce livre a été atteind.");
    }

    @DisplayName("Test good validation of reservation - Loan ongoing")
    @Test
    void checkReservationValidity_NotAlreadyLoanForThisBook() {
        // GIVEN
        initSecurityMock();
        Loan loan = new Loan();
        Book book = new Book();
        CopyBook copyBook = new CopyBook();
        book.setId(1);
        copyBook.setBook(book);
        loan.setCopyBook(copyBook);
        loan.setStatus(Status.ONGOING);
        given(loanRepository.findAllLoansNotFinishByEmail("user@mail.fr")).willReturn(Collections.singletonList(loan));
        BookDto bookDto = new BookDto();
        ReservationDto reservationDto = new ReservationDto();
        bookDto.setId(1);
        bookDto.setCopies(3);
        reservationDto.setBook(bookDto);

        // WHEN
        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> reservationServiceUnderTest.checkReservationValidity(reservationDto));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("Vous avez déjà un emprunt en cours pour ce livre.");
    }

    @DisplayName("Test good validation of reservation - GoodReservation")
    @Test
    void checkReservationValidity_GoodReservationNoFunctionalException() {
        // GIVEN
        initSecurityMock();
        given(reservationRepository.getNumberOfReservation(1)).willReturn(3);
        given(loanRepository.findAllLoansNotFinishByEmail("user@mail.fr")).willReturn(new ArrayList<>());
        BookDto bookDto = new BookDto();
        ReservationDto reservationDto = new ReservationDto();
        bookDto.setId(1);
        bookDto.setCopies(3);
        reservationDto.setBook(bookDto);

        // WHEN & THEN
        reservationServiceUnderTest.checkReservationValidity(reservationDto);

    }

    @DisplayName("Test false is return when not priority")
    @Test
    void isPriority_returnFalseWhenNotPriority(){
        // GIVEN
        Client client = new Client();
        client.setId(2);
        ClientDto clientDto = new ClientDto();
        clientDto.setId(2);
        Page<Client> clientPage = new PageImpl(Collections.singletonList(client));
        given(reservationRepository.findFirstClientFromBookId(1, PageRequest.of(0, 1))).willReturn(clientPage);
        given(clientMapper.fromClientToDto(client)).willReturn(clientDto);

        // WHEN
        boolean result = reservationServiceUnderTest.isPriority(1,1);

        // THEN
        assertThat(result).isEqualTo(false);
    }

    @DisplayName("Test true is return when priority")
    @Test
    void isPriority_returnTrueWhenPriority(){
        // GIVEN
        Client client = new Client();
        client.setId(1);
        ClientDto clientDto = new ClientDto();
        clientDto.setId(1);
        Page<Client> clientPage = new PageImpl(Collections.singletonList(client));
        given(reservationRepository.findFirstClientFromBookId(1, PageRequest.of(0, 1))).willReturn(clientPage);
        given(clientMapper.fromClientToDto(client)).willReturn(clientDto);

        // WHEN
        boolean result = reservationServiceUnderTest.isPriority(1,1);

        // THEN
        assertThat(result).isEqualTo(true);
    }

}