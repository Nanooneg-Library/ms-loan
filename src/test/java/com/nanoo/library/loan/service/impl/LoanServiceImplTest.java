package com.nanoo.library.loan.service.impl;

import com.nanoo.library.commonpackage.exception.FunctionalException;
import com.nanoo.library.loan.database.ClientRepository;
import com.nanoo.library.loan.database.LoanRepository;
import com.nanoo.library.loan.model.dto.*;
import com.nanoo.library.loan.model.entities.Loan;
import com.nanoo.library.loan.model.mapper.ClientMapper;
import com.nanoo.library.loan.model.mapper.CopyBookMapper;
import com.nanoo.library.loan.model.mapper.LoanMapper;
import com.nanoo.library.loan.service.contract.ReservationService;
import com.nanoo.library.loan.web.proxy.FeignProxy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class LoanServiceImplTest {

    @Mock LoanRepository loanRepository;
    @Mock ClientRepository clientRepository;
    @Mock LoanMapper loanMapper;
    @Mock ClientMapper clientMapper;
    @Mock CopyBookMapper copyBookMapper;
    @Mock ReservationService reservationService;
    @Mock FeignProxy proxy;

    private LoanServiceImpl loanServiceUnderTest;

    @BeforeEach
    public void initMock() {
        loanServiceUnderTest = new LoanServiceImpl(loanRepository,clientRepository,loanMapper,clientMapper,copyBookMapper,reservationService,proxy);
    }

    @DisplayName("Test impossibility of loan extension when outdated")
    @Test
    void extendLoan_ImpossibleWhenOutdated() {
        // GIVEN
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DAY_OF_WEEK,-2);
        Loan loan = new Loan();
        loan.setExtended(false);
        loan.setExpectedReturnDate(c.getTime());
        given(loanRepository.findById(1)).willReturn(Optional.of(loan));

        // WHEN
        Object result = loanServiceUnderTest.extendLoan(1);

        // THEN
        assertThat(result).isNull();
    }

    @DisplayName("Test possibility of loan extension when not outdated")
    @Test
    void extendLoan_PossibleWhenNotOutdated() {
        // GIVEN
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DAY_OF_WEEK,2);
        Loan loan = new Loan();
        loan.setExtended(false);
        loan.setExpectedReturnDate(c.getTime());
        given(loanRepository.findById(1)).willReturn(Optional.of(loan));
        given(loanRepository.save(any(Loan.class))).willReturn(loan);
        given(loanMapper.fromLoanToDtoWithCopyBookInfo(loan)).willReturn(new LoanWithCopyBookInfoDto());

        // WHEN
        Object result = loanServiceUnderTest.extendLoan(1);

        // THEN
        assertThat(result).isNotEqualTo(null);
    }

    @DisplayName("Test impossibility of loan creation when not priority")
    @Test
    void createLoan_ImpossibleWhenNotPriority() {
        // GIVEN
        ClientDto client = new ClientDto();
        client.setId(1);
        BookDto book = new BookDto();
        book.setId(1);
        CopyBookDto copyBook = new CopyBookDto();
        copyBook.setBook(book);
        LoanToHandleDto loan = new LoanToHandleDto();
        loan.setClient(client);
        loan.setCopyBook(copyBook);
        given(reservationService.isPriority(1,1)).willReturn(false);

        // WHEN
        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> loanServiceUnderTest.createLoan(loan));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("L'utilisateur n'est pas prioritaire pour emprunter ce livre.");
    }

    @DisplayName("Test possibility of loan creation when priority")
    @Test
    void createLoan_PossibleWhenPriority() {
        // GIVEN
        ClientDto client = new ClientDto();
        client.setId(1);
        BookDto book = new BookDto();
        book.setId(1);
        CopyBookDto copyBook = new CopyBookDto();
        copyBook.setBook(book);
        LoanToHandleDto loan = new LoanToHandleDto();
        loan.setClient(client);
        loan.setCopyBook(copyBook);
        given(reservationService.isPriority(1,1)).willReturn(true);

        // WHEN & THEN
        loanServiceUnderTest.createLoan(loan);
    }

}