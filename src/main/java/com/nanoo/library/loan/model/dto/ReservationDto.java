package com.nanoo.library.loan.model.dto;

import com.nanoo.library.loan.model.entities.Book;
import com.nanoo.library.loan.model.entities.Client;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author nanoo
 * @created 05/04/2020 - 11:21
 */
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReservationDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private BookDto book;
    private ClientDto client;
    private Date reservationDate;
    private Date notificationDate;
    private int placeInQueue;
    private Date closestReturnDate;

}
