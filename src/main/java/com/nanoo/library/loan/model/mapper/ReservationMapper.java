package com.nanoo.library.loan.model.mapper;

import com.nanoo.library.loan.model.dto.ReservationDto;
import com.nanoo.library.loan.model.entities.Reservation;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * @author nanoo
 * @created 05/04/2020 - 11:26
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = {BookMapper.class,ClientMapper.class})
public interface ReservationMapper {

    Reservation fromDtoToReservation (ReservationDto reservationDto);

    ReservationDto fromReservationToDto (Reservation reservation);

    List<Reservation> fromReservationDtoListToReservations (List<ReservationDto> reservationDtoList);

    List<ReservationDto> fromReservationsToDtos (List<Reservation> reservations);

}
