package com.nanoo.library.loan.web.proxy;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author nanoo
 * @create 27/01/2020 - 22:38
 */
@FeignClient(name = "server-gateway", url = "localhost:8763")
public interface FeignProxy {
    
    /* ================================ */
    /* === No Authentication needed === */
    /* ================================ */
    
    @GetMapping("/api-batch/launch/notification")
    void doLoanStatusUpdate();

}
