package com.nanoo.library.loan.web.controller;

import com.nanoo.library.loan.model.dto.ClientDto;
import com.nanoo.library.loan.service.contract.LoanService;
import com.nanoo.library.loan.service.contract.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;

/**
 * @author nanoo
 * @created 07/04/2020 - 11:02
 */
@RestController
@RequestMapping("/technical")
public class TechnicalController {

    private final LoanService loanService;
    private final ReservationService reservationService;

    @Autowired
    public TechnicalController(LoanService loanService, ReservationService reservationService) {
        this.loanService = loanService;
        this.reservationService = reservationService;
    }

    @PutMapping("/edit/account")
    public ClientDto editAccountInfo (@RequestBody ClientDto clientDto){

        return loanService.editAccountInfo(clientDto);

    }

    @GetMapping("/get/emails")
    public Map<String, Date> getEmails (){

        return loanService.getOutdatedLoansEmailAccount();

    }

    @GetMapping("/loanStatus")
    public int updateStatus(){

        return loanService.editLoanStatus();

    }

    @GetMapping("/reservationOutdated")
    public Map<String, String> deleteOutdatedReservations (){

        return reservationService.deleteOutdatedReservationAndGetNext();

    }

    @GetMapping("/getFirstClient/{bookId}")
    public ClientDto getFirstClient(@PathVariable("bookId") int bookId){
        return reservationService.getFirstReservation(bookId);
    }

}

