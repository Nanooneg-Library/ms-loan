package com.nanoo.library.loan.web.controller;

import com.nanoo.library.loan.model.dto.LoanWithAccountInfoDto;
import com.nanoo.library.loan.model.dto.LoanWithCopyBookInfoDto;
import com.nanoo.library.loan.service.contract.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author nanoo
 * @create 23/11/2019 - 21:00
 */
@RestController
public class LoanConsultController {
    
    private final LoanService loanService;
    
    @Autowired
    public LoanConsultController(LoanService loanService) {
        this.loanService = loanService;
    }
    
    @GetMapping("/consult/allLoans")
    public List<LoanWithAccountInfoDto> listAllLoans (){
        
        return loanService.getLoanList();
        
    }
    
    @GetMapping("/consult/loans/{userId}/{loanProperty}")
    public List<LoanWithCopyBookInfoDto> listUserLoans(@PathVariable int userId, @PathVariable String loanProperty){
        
        return loanService.getUserLoanList(userId,loanProperty);
        
    }
    
}
