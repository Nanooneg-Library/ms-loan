package com.nanoo.library.loan.web.controller;

import com.nanoo.library.commonpackage.exception.FunctionalException;
import com.nanoo.library.loan.model.dto.ClientDto;
import com.nanoo.library.loan.model.dto.ReservationDto;
import com.nanoo.library.loan.service.contract.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author nanoo
 * @created 06/04/2020 - 10:54
 */
@RestController
@RequestMapping("/reservations")
public class ReservationController {

    private final ReservationService reservationService;

    @Autowired
    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping("/all")
    public List<ReservationDto> getReservationList(){
        return reservationService.getReservationList();
    }

    @GetMapping("/{userId}")
    public List<ReservationDto> getUserReservationList(@PathVariable("userId") int userId){
        return reservationService.getUserReservationList(userId);
    }

    @PutMapping("/add/{bookId}")
    public ReservationDto addReservation (@PathVariable("bookId") int bookId, @RequestBody ClientDto clientDto) throws FunctionalException {
        return reservationService.createReservation(bookId,clientDto);
    }

    @PutMapping("/remove/{reservationId}")
    public void removeReservation(@PathVariable("reservationId") int reservationId){
        reservationService.deleteReservation(reservationId);
    }
}
