package com.nanoo.library.loan.database;

import com.nanoo.library.loan.model.entities.Client;
import com.nanoo.library.loan.model.entities.Reservation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @author nanoo
 * @created 05/04/2020 - 14:13
 */
@Repository
public interface ReservationRepository extends JpaRepository<Reservation,Integer> {

    @Query(value = "SELECT r FROM Reservation r WHERE r.client.id = :clientId ")
    List<Reservation> findAllByClientId(@Param("clientId") int clientId);

    @Query(value = "SELECT r.client FROM Reservation r " +
                   "WHERE r.book.id = :bookId " +
                   "ORDER BY r.reservationDate ASC")
    Page<Client> findFirstClientFromBookId(@Param("bookId") int bookId, Pageable pageable);

    @Query(value = "SELECT r FROM Reservation r " +
                   "WHERE r.notificationDate < :outdated ")
    List<Reservation> findAllOutdated(@Param("outdated") Date outdated);

    @Modifying
    @Query(value = "UPDATE Reservation AS r " +
                   "SET r.notificationDate = CURRENT_DATE " +
                   "WHERE r.client.id = :clientId AND r.book.id = :bookId")
    void updateReservation(@Param("clientId") Integer clientId, @Param("bookId") Integer bookId);

    @Query(value = "SELECT COUNT (r) FROM Reservation r " +
                   "WHERE r.book.id = :bookId")
    int getNumberOfReservation(@Param("bookId") Integer bookId);

    @Query(value = "SELECT r FROM Reservation r " +
                   "WHERE r.book.id = :bookId")
    List<Reservation> findAllByBookId(@Param("bookId") Integer id);
}