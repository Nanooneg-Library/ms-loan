package com.nanoo.library.loan.service.contract;

import com.nanoo.library.commonpackage.exception.FunctionalException;
import com.nanoo.library.loan.model.dto.ClientDto;
import com.nanoo.library.loan.model.dto.ReservationDto;

import java.util.List;
import java.util.Map;

/**
 * @author nanoo
 * @created 05/04/2020 - 14:14
 */
public interface ReservationService {

    List<ReservationDto> getReservationList();

    List<ReservationDto> getUserReservationList(int userId);

    ReservationDto createReservation(int bookId, ClientDto clientDto) throws FunctionalException;

    void deleteReservation(int reservationId);

    ClientDto getFirstReservation(int bookId);

    Map<String, String> deleteOutdatedReservationAndGetNext();

    boolean isPriority(int clientId, int bookId);
}
